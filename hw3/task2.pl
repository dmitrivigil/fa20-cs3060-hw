accMax([H|T],A,Max) :-
    H > A,
    accMax(T,H,Max).
 
accMax([H|T],A,Max) :-
    H =< A,
    accMax(T,A,Max).
 
accMax([],A,A).

accMax([1,0,5,4],0,_5810)  
 
accMax([0,-5,4],1,_5810) 